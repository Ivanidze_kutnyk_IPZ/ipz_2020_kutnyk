#include "total_main.h"
#include "ui_total_main.h"
#include "search_customer.h"
#include "exit.h"
#include "QString"
#include "QMessageBox"
#include "QCloseEvent"


total_main::total_main(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::total_main)
{
    ui->setupUi(this);
}

total_main::~total_main()
{
    delete ui;
}

//Очищення поля SMS_TEXT
void total_main::on_Clear_b_clicked()
{
    ui->sms_text->clear();
}
//Надсилання смс з поля SMS_TEXT
void total_main::on_Send_b_clicked()
{
    QString text = ui->sms_text->toPlainText();
    ui->sms_text->clear();
}
//Виклик вікна пошуку користувача
void total_main::on_Search_b_clicked()
{
  search_customer cust;
  cust.setModal(true);
  cust.setMinimumSize(800,480);
  cust.setMaximumSize(800,480);
  cust.setWindowTitle("Searching Customers");
  cust.exec();
}

void total_main::on_EXIT_B_clicked()
{
 this->close();
}
void total_main::closeEvent(QCloseEvent *event)
{
    event->ignore();
        QMessageBox exit;
        exit.setWindowTitle("Бажаєте вийти?");
        exit.setStyleSheet("color:white;background-color:rgb(0, 0, 140);");
        QAbstractButton *yes = exit.addButton("ТАК",QMessageBox::YesRole);
        QAbstractButton *no = exit.addButton("НІ",QMessageBox::NoRole);
        exit.setText("Впевнені?");
        exit.exec();
        if(exit.clickedButton() == yes) event->accept();

}
