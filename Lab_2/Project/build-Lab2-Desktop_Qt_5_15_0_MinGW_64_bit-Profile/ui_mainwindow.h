/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *About_Button;
    QPushButton *EXIT_Button;
    QLabel *Total_name;
    QLabel *Login_L;
    QLabel *Pass_L;
    QPushButton *IN_b;
    QPushButton *Reg_b;
    QLineEdit *login_l;
    QLineEdit *pass_l;
    QPushButton *Connect_B;
    QMenuBar *menubar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 480);
        MainWindow->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 0, 48);\n"
""));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        About_Button = new QPushButton(centralwidget);
        About_Button->setObjectName(QString::fromUtf8("About_Button"));
        About_Button->setGeometry(QRect(700, 430, 80, 21));
        QFont font;
        font.setBold(true);
        font.setItalic(false);
        font.setWeight(75);
        About_Button->setFont(font);
        About_Button->setStyleSheet(QString::fromUtf8("color: white;\n"
"background-color:rgb(0, 0, 130);"));
        EXIT_Button = new QPushButton(centralwidget);
        EXIT_Button->setObjectName(QString::fromUtf8("EXIT_Button"));
        EXIT_Button->setGeometry(QRect(20, 430, 80, 21));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        EXIT_Button->setFont(font1);
        EXIT_Button->setStyleSheet(QString::fromUtf8("color: white;\n"
"background-color:rgb(0, 0, 130);"));
        Total_name = new QLabel(centralwidget);
        Total_name->setObjectName(QString::fromUtf8("Total_name"));
        Total_name->setGeometry(QRect(170, 20, 420, 120));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Vivaldi"));
        font2.setPointSize(72);
        font2.setBold(true);
        font2.setItalic(true);
        font2.setWeight(75);
        Total_name->setFont(font2);
        Total_name->setStyleSheet(QString::fromUtf8("color: white;"));
        Total_name->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);
        Login_L = new QLabel(centralwidget);
        Login_L->setObjectName(QString::fromUtf8("Login_L"));
        Login_L->setGeometry(QRect(276, 150, 42, 18));
        QFont font3;
        font3.setPointSize(11);
        font3.setBold(true);
        font3.setWeight(75);
        Login_L->setFont(font3);
        Login_L->setStyleSheet(QString::fromUtf8("color:white;"));
        Pass_L = new QLabel(centralwidget);
        Pass_L->setObjectName(QString::fromUtf8("Pass_L"));
        Pass_L->setGeometry(QRect(276, 200, 81, 21));
        Pass_L->setFont(font3);
        Pass_L->setStyleSheet(QString::fromUtf8("color:white;"));
        IN_b = new QPushButton(centralwidget);
        IN_b->setObjectName(QString::fromUtf8("IN_b"));
        IN_b->setGeometry(QRect(430, 300, 81, 41));
        IN_b->setFont(font3);
        IN_b->setStyleSheet(QString::fromUtf8("color: white;\n"
"Background-color: rgb(0, 0, 127);"));
        Reg_b = new QPushButton(centralwidget);
        Reg_b->setObjectName(QString::fromUtf8("Reg_b"));
        Reg_b->setGeometry(QRect(260, 300, 141, 41));
        Reg_b->setFont(font3);
        Reg_b->setStyleSheet(QString::fromUtf8("color: white;\n"
"Background-color: rgb(0, 0, 127);"));
        login_l = new QLineEdit(centralwidget);
        login_l->setObjectName(QString::fromUtf8("login_l"));
        login_l->setGeometry(QRect(276, 170, 221, 22));
        login_l->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 116);"));
        pass_l = new QLineEdit(centralwidget);
        pass_l->setObjectName(QString::fromUtf8("pass_l"));
        pass_l->setGeometry(QRect(276, 220, 221, 22));
        pass_l->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 116);"));
        Connect_B = new QPushButton(centralwidget);
        Connect_B->setObjectName(QString::fromUtf8("Connect_B"));
        Connect_B->setGeometry(QRect(690, 0, 91, 31));
        Connect_B->setStyleSheet(QString::fromUtf8("color: white;\n"
"Background-color: rgb(0, 0, 127);"));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 20));
        MainWindow->setMenuBar(menubar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        About_Button->setText(QCoreApplication::translate("MainWindow", "About", nullptr));
        EXIT_Button->setText(QCoreApplication::translate("MainWindow", "EXIT", nullptr));
        Total_name->setText(QCoreApplication::translate("MainWindow", "FreeGram", nullptr));
        Login_L->setText(QCoreApplication::translate("MainWindow", "\320\233\320\276\320\263\321\226\320\275", nullptr));
        Pass_L->setText(QCoreApplication::translate("MainWindow", "\320\237\320\260\321\200\320\276\320\273\321\214", nullptr));
        IN_b->setText(QCoreApplication::translate("MainWindow", "\320\243\320\262\321\226\320\271\321\202\320\270", nullptr));
        Reg_b->setText(QCoreApplication::translate("MainWindow", "\320\240\320\265\321\224\321\201\321\202\321\200\320\260\321\206\321\226\321\217", nullptr));
        login_l->setText(QCoreApplication::translate("MainWindow", "TEST", nullptr));
        pass_l->setText(QCoreApplication::translate("MainWindow", "1234", nullptr));
        Connect_B->setText(QCoreApplication::translate("MainWindow", "Connect", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
