/********************************************************************************
** Form generated from reading UI file 'exit.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXIT_H
#define UI_EXIT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_EXIT
{
public:
    QLabel *label;
    QPushButton *NO_B;
    QPushButton *YES_B;

    void setupUi(QDialog *EXIT)
    {
        if (EXIT->objectName().isEmpty())
            EXIT->setObjectName(QString::fromUtf8("EXIT"));
        EXIT->resize(300, 200);
        EXIT->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 0, 48);\n"
""));
        label = new QLabel(EXIT);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 20, 271, 31));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setStyleSheet(QString::fromUtf8("color:white;\n"
""));
        NO_B = new QPushButton(EXIT);
        NO_B->setObjectName(QString::fromUtf8("NO_B"));
        NO_B->setGeometry(QRect(160, 100, 91, 31));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        NO_B->setFont(font1);
        NO_B->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 116)"));
        YES_B = new QPushButton(EXIT);
        YES_B->setObjectName(QString::fromUtf8("YES_B"));
        YES_B->setGeometry(QRect(50, 100, 91, 31));
        YES_B->setFont(font1);
        YES_B->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 116)"));

        retranslateUi(EXIT);

        QMetaObject::connectSlotsByName(EXIT);
    } // setupUi

    void retranslateUi(QDialog *EXIT)
    {
        EXIT->setWindowTitle(QCoreApplication::translate("EXIT", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("EXIT", "\320\222\320\270 \320\264\321\226\320\271\321\201\320\275\320\276 \320\261\320\260\320\266\320\260\321\224\321\202\320\265 \320\262\320\270\320\271\321\202\320\270?", nullptr));
        NO_B->setText(QCoreApplication::translate("EXIT", "\320\235\320\206", nullptr));
        YES_B->setText(QCoreApplication::translate("EXIT", "\320\242\320\220\320\232", nullptr));
    } // retranslateUi

};

namespace Ui {
    class EXIT: public Ui_EXIT {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXIT_H
