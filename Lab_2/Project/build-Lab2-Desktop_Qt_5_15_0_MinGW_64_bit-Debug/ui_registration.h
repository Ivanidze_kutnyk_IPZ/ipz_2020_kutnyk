/********************************************************************************
** Form generated from reading UI file 'registration.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGISTRATION_H
#define UI_REGISTRATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>

QT_BEGIN_NAMESPACE

class Ui_registration
{
public:
    QLabel *label;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QSplitter *splitter_3;
    QSplitter *splitter_2;
    QLabel *label_2;
    QLabel *label_3;
    QSplitter *splitter;
    QLineEdit *login_r;
    QLineEdit *pass_r;
    QPushButton *pushButton_back;
    QPushButton *reg_in;

    void setupUi(QDialog *registration)
    {
        if (registration->objectName().isEmpty())
            registration->setObjectName(QString::fromUtf8("registration"));
        registration->resize(800, 480);
        registration->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 0, 48);\n"
""));
        label = new QLabel(registration);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(187, 20, 451, 141));
        QFont font;
        font.setFamily(QString::fromUtf8("Vivaldi"));
        font.setPointSize(80);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setStyleSheet(QString::fromUtf8("color:white;"));
        groupBox = new QGroupBox(registration);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(240, 170, 341, 111));
        QFont font1;
        font1.setPointSize(15);
        font1.setBold(true);
        font1.setWeight(75);
        font1.setKerning(true);
        groupBox->setFont(font1);
        groupBox->setStyleSheet(QString::fromUtf8("color:white;"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        splitter_3 = new QSplitter(groupBox);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setOrientation(Qt::Horizontal);
        splitter_2 = new QSplitter(splitter_3);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Vertical);
        label_2 = new QLabel(splitter_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font2;
        font2.setPointSize(14);
        font2.setBold(true);
        font2.setWeight(75);
        label_2->setFont(font2);
        label_2->setStyleSheet(QString::fromUtf8("color:white;"));
        splitter_2->addWidget(label_2);
        label_3 = new QLabel(splitter_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font2);
        label_3->setStyleSheet(QString::fromUtf8("color:white;"));
        splitter_2->addWidget(label_3);
        splitter_3->addWidget(splitter_2);
        splitter = new QSplitter(splitter_3);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        login_r = new QLineEdit(splitter);
        login_r->setObjectName(QString::fromUtf8("login_r"));
        login_r->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 116);"));
        splitter->addWidget(login_r);
        pass_r = new QLineEdit(splitter);
        pass_r->setObjectName(QString::fromUtf8("pass_r"));
        pass_r->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 116);"));
        splitter->addWidget(pass_r);
        splitter_3->addWidget(splitter);

        gridLayout->addWidget(splitter_3, 0, 0, 1, 1);

        pushButton_back = new QPushButton(registration);
        pushButton_back->setObjectName(QString::fromUtf8("pushButton_back"));
        pushButton_back->setGeometry(QRect(20, 420, 61, 31));
        QFont font3;
        font3.setFamily(QString::fromUtf8("SimHei"));
        font3.setPointSize(23);
        font3.setBold(true);
        font3.setWeight(75);
        pushButton_back->setFont(font3);
        pushButton_back->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 116);"));
        reg_in = new QPushButton(registration);
        reg_in->setObjectName(QString::fromUtf8("reg_in"));
        reg_in->setGeometry(QRect(327, 290, 151, 41));
        reg_in->setFont(font2);
        reg_in->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 116);"));

        retranslateUi(registration);

        QMetaObject::connectSlotsByName(registration);
    } // setupUi

    void retranslateUi(QDialog *registration)
    {
        registration->setWindowTitle(QCoreApplication::translate("registration", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("registration", "Registration ", nullptr));
        groupBox->setTitle(QCoreApplication::translate("registration", "Infotmation", nullptr));
        label_2->setText(QCoreApplication::translate("registration", "\320\233\320\276\320\263\321\226\320\275", nullptr));
        label_3->setText(QCoreApplication::translate("registration", "\320\237\320\260\321\200\320\276\320\273\321\214", nullptr));
        login_r->setText(QString());
        pass_r->setText(QString());
        pushButton_back->setText(QCoreApplication::translate("registration", "<-", nullptr));
        reg_in->setText(QCoreApplication::translate("registration", "\320\240\320\265\321\224\321\201\321\202\321\200\320\260\321\206\321\226\321\217", nullptr));
    } // retranslateUi

};

namespace Ui {
    class registration: public Ui_registration {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGISTRATION_H
