#ifndef TOTAL_MAIN_H
#define TOTAL_MAIN_H

#include <QDialog>

namespace Ui {
class total_main;
}

class total_main : public QDialog
{
    Q_OBJECT

public:
    explicit total_main(QWidget *parent = nullptr);
    ~total_main();

private slots:

    void on_Clear_b_clicked();

    void on_Send_b_clicked();

    void on_Search_b_clicked();

    void on_EXIT_B_clicked();

private:
    Ui::total_main *ui;
    void closeEvent(QCloseEvent *event);
};

#endif // TOTAL_MAIN_H
