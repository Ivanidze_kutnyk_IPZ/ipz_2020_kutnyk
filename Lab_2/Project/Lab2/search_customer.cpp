#include "search_customer.h"
#include "ui_search_customer.h"
#include "QString"

search_customer::search_customer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::search_customer)
{
    ui->setupUi(this);
}

search_customer::~search_customer()
{
    delete ui;
}

//Кнопка очищення ПОЛІВ
void search_customer::on_Clear_B_clicked()
{
    //Info_In
    ui->name_in->clear();
    ui->second_name_in->clear();
    ui->ID_IN->clear();
    //Info_Out
    ui->name_out->clear();
    ui->second_name_out->clear();
    ui->data_out->clear();
    ui->nik_out->clear();
    ui->email_out->clear();
    ui->ID_out->clear();
    ui->stat_info->clear();
}
//Кнопка закривання ВІКНА
void search_customer::on_Exit_B_clicked()
{
    this->close();
}

void search_customer::on_search_B_clicked()
{
    //Заповнення полів
    QString name = ui->name_in->text();
    QString second_name = ui->second_name_in->text();
    QString ID = ui->ID_IN->text();
    //Перевірка на наявність користувача
    if (name == "Ivan" && second_name == "Kutnyk") {
        ui->name_out->setText(name);
        ui->second_name_out->setText(second_name);
        ui->ID_out->setText("567");
        ui->data_out->setText("06.07.2001");
        ui->email_out->setText("Kutnykvana6@gmail.com");
        ui->nik_out->setText("Ivanidze_kutnyk");
        ui->stat_info->setText("Користувач знайдений");
}
    else if(ID == "567"){
        ui->name_out->setText("Ivan");
        ui->second_name_out->setText("Kutnyk");
        ui->ID_out->setText(ID);
        ui->data_out->setText("06.07.2001");
        ui->email_out->setText("Kutnykvana6@gmail.com");
        ui->nik_out->setText("Ivanidze_kutnyk");
        ui->stat_info->setText("Користувач знайдений");
    }
    else if (name == NULL && second_name == NULL && ID == NULL){
        ui->stat_info->setText("Вкажіть дані для пошуку");
    }
    else{
         ui->stat_info->setText("Користувач не знайдений");
    }
}
