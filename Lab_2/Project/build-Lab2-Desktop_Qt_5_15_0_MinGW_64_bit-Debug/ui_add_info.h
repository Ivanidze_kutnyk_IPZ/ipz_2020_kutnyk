/********************************************************************************
** Form generated from reading UI file 'add_info.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADD_INFO_H
#define UI_ADD_INFO_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>

QT_BEGIN_NAMESPACE

class Ui_add_info
{
public:
    QGroupBox *INFO_OUT;
    QSplitter *splitter_2;
    QLabel *sec_name_L;
    QLineEdit *sec_name;
    QSplitter *splitter_3;
    QLabel *date_L;
    QLineEdit *date;
    QSplitter *splitter_4;
    QLabel *nick_L;
    QLineEdit *nick;
    QSplitter *splitter_5;
    QLabel *email_L;
    QLineEdit *email;
    QSplitter *splitter_6;
    QSplitter *splitter;
    QLabel *name_L;
    QLineEdit *name_in;
    QLabel *logo;
    QPushButton *Exit_B;
    QPushButton *Red;
    QSplitter *splitter_7;
    QLabel *stat_L;
    QLabel *stat_info;

    void setupUi(QDialog *add_info)
    {
        if (add_info->objectName().isEmpty())
            add_info->setObjectName(QString::fromUtf8("add_info"));
        add_info->resize(800, 480);
        add_info->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 0, 48);\n"
""));
        INFO_OUT = new QGroupBox(add_info);
        INFO_OUT->setObjectName(QString::fromUtf8("INFO_OUT"));
        INFO_OUT->setGeometry(QRect(130, 190, 511, 201));
        QFont font;
        font.setPointSize(15);
        font.setBold(true);
        font.setWeight(75);
        INFO_OUT->setFont(font);
        INFO_OUT->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);"));
        splitter_2 = new QSplitter(INFO_OUT);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setGeometry(QRect(10, 70, 491, 23));
        splitter_2->setOrientation(Qt::Horizontal);
        sec_name_L = new QLabel(splitter_2);
        sec_name_L->setObjectName(QString::fromUtf8("sec_name_L"));
        sec_name_L->setEnabled(true);
        sec_name_L->setMinimumSize(QSize(187, 23));
        sec_name_L->setMaximumSize(QSize(313, 16777215));
        QFont font1;
        font1.setPointSize(14);
        sec_name_L->setFont(font1);
        sec_name_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_2->addWidget(sec_name_L);
        sec_name = new QLineEdit(splitter_2);
        sec_name->setObjectName(QString::fromUtf8("sec_name"));
        sec_name->setMinimumSize(QSize(301, 23));
        sec_name->setMaximumSize(QSize(301, 23));
        sec_name->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_2->addWidget(sec_name);
        splitter_3 = new QSplitter(INFO_OUT);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setGeometry(QRect(10, 100, 491, 23));
        splitter_3->setOrientation(Qt::Horizontal);
        date_L = new QLabel(splitter_3);
        date_L->setObjectName(QString::fromUtf8("date_L"));
        date_L->setEnabled(true);
        date_L->setMinimumSize(QSize(187, 23));
        date_L->setMaximumSize(QSize(313, 16777215));
        date_L->setFont(font1);
        date_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_3->addWidget(date_L);
        date = new QLineEdit(splitter_3);
        date->setObjectName(QString::fromUtf8("date"));
        date->setMinimumSize(QSize(301, 23));
        date->setMaximumSize(QSize(301, 23));
        date->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_3->addWidget(date);
        splitter_4 = new QSplitter(INFO_OUT);
        splitter_4->setObjectName(QString::fromUtf8("splitter_4"));
        splitter_4->setGeometry(QRect(10, 130, 491, 23));
        splitter_4->setOrientation(Qt::Horizontal);
        nick_L = new QLabel(splitter_4);
        nick_L->setObjectName(QString::fromUtf8("nick_L"));
        nick_L->setEnabled(true);
        nick_L->setMinimumSize(QSize(187, 23));
        nick_L->setMaximumSize(QSize(313, 16777215));
        nick_L->setFont(font1);
        nick_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_4->addWidget(nick_L);
        nick = new QLineEdit(splitter_4);
        nick->setObjectName(QString::fromUtf8("nick"));
        nick->setMinimumSize(QSize(301, 23));
        nick->setMaximumSize(QSize(301, 23));
        nick->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_4->addWidget(nick);
        splitter_5 = new QSplitter(INFO_OUT);
        splitter_5->setObjectName(QString::fromUtf8("splitter_5"));
        splitter_5->setGeometry(QRect(10, 160, 491, 23));
        splitter_5->setOrientation(Qt::Horizontal);
        email_L = new QLabel(splitter_5);
        email_L->setObjectName(QString::fromUtf8("email_L"));
        email_L->setEnabled(true);
        email_L->setMinimumSize(QSize(187, 23));
        email_L->setMaximumSize(QSize(313, 16777215));
        email_L->setFont(font1);
        email_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_5->addWidget(email_L);
        email = new QLineEdit(splitter_5);
        email->setObjectName(QString::fromUtf8("email"));
        email->setMinimumSize(QSize(301, 23));
        email->setMaximumSize(QSize(301, 23));
        email->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_5->addWidget(email);
        splitter_6 = new QSplitter(INFO_OUT);
        splitter_6->setObjectName(QString::fromUtf8("splitter_6"));
        splitter_6->setGeometry(QRect(10, 190, 301, 23));
        splitter_6->setOrientation(Qt::Horizontal);
        splitter = new QSplitter(INFO_OUT);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setGeometry(QRect(10, 40, 491, 23));
        splitter->setOrientation(Qt::Horizontal);
        name_L = new QLabel(splitter);
        name_L->setObjectName(QString::fromUtf8("name_L"));
        name_L->setEnabled(true);
        name_L->setMinimumSize(QSize(187, 23));
        name_L->setMaximumSize(QSize(313, 16777215));
        name_L->setFont(font1);
        name_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter->addWidget(name_L);
        name_in = new QLineEdit(splitter);
        name_in->setObjectName(QString::fromUtf8("name_in"));
        name_in->setMinimumSize(QSize(301, 23));
        name_in->setMaximumSize(QSize(301, 23));
        name_in->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter->addWidget(name_in);
        logo = new QLabel(add_info);
        logo->setObjectName(QString::fromUtf8("logo"));
        logo->setGeometry(QRect(60, 30, 698, 81));
        QFont font2;
        font2.setPointSize(40);
        font2.setBold(true);
        font2.setWeight(75);
        logo->setFont(font2);
        logo->setStyleSheet(QString::fromUtf8("color:white;"));
        Exit_B = new QPushButton(add_info);
        Exit_B->setObjectName(QString::fromUtf8("Exit_B"));
        Exit_B->setGeometry(QRect(10, 120, 81, 21));
        QFont font3;
        font3.setPointSize(14);
        font3.setBold(true);
        font3.setWeight(75);
        Exit_B->setFont(font3);
        Exit_B->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        Red = new QPushButton(add_info);
        Red->setObjectName(QString::fromUtf8("Red"));
        Red->setGeometry(QRect(490, 400, 151, 31));
        QFont font4;
        font4.setPointSize(12);
        font4.setBold(true);
        font4.setWeight(75);
        Red->setFont(font4);
        Red->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_7 = new QSplitter(add_info);
        splitter_7->setObjectName(QString::fromUtf8("splitter_7"));
        splitter_7->setGeometry(QRect(10, 160, 322, 23));
        splitter_7->setOrientation(Qt::Horizontal);
        stat_L = new QLabel(splitter_7);
        stat_L->setObjectName(QString::fromUtf8("stat_L"));
        stat_L->setMaximumSize(QSize(84, 16777215));
        stat_L->setFont(font3);
        stat_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_7->addWidget(stat_L);
        stat_info = new QLabel(splitter_7);
        stat_info->setObjectName(QString::fromUtf8("stat_info"));
        stat_info->setMinimumSize(QSize(238, 21));
        stat_info->setMaximumSize(QSize(238, 21));
        QFont font5;
        font5.setPointSize(10);
        font5.setBold(true);
        font5.setWeight(75);
        stat_info->setFont(font5);
        stat_info->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_7->addWidget(stat_info);

        retranslateUi(add_info);

        QMetaObject::connectSlotsByName(add_info);
    } // setupUi

    void retranslateUi(QDialog *add_info)
    {
        add_info->setWindowTitle(QCoreApplication::translate("add_info", "Dialog", nullptr));
        INFO_OUT->setTitle(QCoreApplication::translate("add_info", "\320\206\320\275\321\204\320\276\321\200\320\274\320\260\321\206\321\226\321\217", nullptr));
        sec_name_L->setText(QCoreApplication::translate("add_info", "\320\237\321\200\321\226\320\267\320\262\320\270\321\211\320\265:", nullptr));
        date_L->setText(QCoreApplication::translate("add_info", "\320\224\320\260\321\202\320\260 \320\275\320\260\321\200\320\276\320\264\320\266\320\265\320\275\320\275\321\217:", nullptr));
        nick_L->setText(QCoreApplication::translate("add_info", "\320\235\321\226\320\272\320\275\320\265\320\271\320\274:", nullptr));
        email_L->setText(QCoreApplication::translate("add_info", "\320\225\320\273\320\265\320\272\321\202\321\200\320\276\320\275\320\275\320\260 \320\277\320\276\321\210\321\202\320\260:", nullptr));
        name_L->setText(QCoreApplication::translate("add_info", "\320\206\320\274'\321\217:", nullptr));
        logo->setText(QCoreApplication::translate("add_info", "\320\240\320\265\320\264\320\260\320\263\321\203\320\262\320\260\320\275\320\275\321\217 \321\226\320\275\321\204\320\276\321\200\320\274\320\260\321\206\321\226\321\227", nullptr));
        Exit_B->setText(QCoreApplication::translate("add_info", "<--", nullptr));
        Red->setText(QCoreApplication::translate("add_info", "\320\227\320\260\321\200\320\265\321\224\321\201\321\202\321\200\321\203\320\262\320\260\321\202\320\270\321\201\321\214", nullptr));
        stat_L->setText(QCoreApplication::translate("add_info", " \320\241\321\202\320\260\321\202\321\203\321\201: ", nullptr));
        stat_info->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class add_info: public Ui_add_info {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADD_INFO_H
