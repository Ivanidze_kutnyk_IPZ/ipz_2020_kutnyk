/****************************************************************************
** Meta object code from reading C++ file 'total_main.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../Lab_3/Lab2/total_main.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'total_main.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_total_main_t {
    QByteArrayData data[12];
    char stringdata0[185];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_total_main_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_total_main_t qt_meta_stringdata_total_main = {
    {
QT_MOC_LITERAL(0, 0, 10), // "total_main"
QT_MOC_LITERAL(1, 11, 9), // "sockReady"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 8), // "sockDisc"
QT_MOC_LITERAL(4, 31, 18), // "on_Clear_b_clicked"
QT_MOC_LITERAL(5, 50, 17), // "on_Send_b_clicked"
QT_MOC_LITERAL(6, 68, 19), // "on_Search_b_clicked"
QT_MOC_LITERAL(7, 88, 17), // "on_EXIT_B_clicked"
QT_MOC_LITERAL(8, 106, 20), // "on_Connect_B_clicked"
QT_MOC_LITERAL(9, 127, 22), // "on_Select_Cust_clicked"
QT_MOC_LITERAL(10, 150, 20), // "on_refresh_B_clicked"
QT_MOC_LITERAL(11, 171, 13) // "on_ID_clicked"

    },
    "total_main\0sockReady\0\0sockDisc\0"
    "on_Clear_b_clicked\0on_Send_b_clicked\0"
    "on_Search_b_clicked\0on_EXIT_B_clicked\0"
    "on_Connect_B_clicked\0on_Select_Cust_clicked\0"
    "on_refresh_B_clicked\0on_ID_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_total_main[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x0a /* Public */,
       3,    0,   65,    2, 0x0a /* Public */,
       4,    0,   66,    2, 0x08 /* Private */,
       5,    0,   67,    2, 0x08 /* Private */,
       6,    0,   68,    2, 0x08 /* Private */,
       7,    0,   69,    2, 0x08 /* Private */,
       8,    0,   70,    2, 0x08 /* Private */,
       9,    0,   71,    2, 0x08 /* Private */,
      10,    0,   72,    2, 0x08 /* Private */,
      11,    0,   73,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void total_main::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<total_main *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sockReady(); break;
        case 1: _t->sockDisc(); break;
        case 2: _t->on_Clear_b_clicked(); break;
        case 3: _t->on_Send_b_clicked(); break;
        case 4: _t->on_Search_b_clicked(); break;
        case 5: _t->on_EXIT_B_clicked(); break;
        case 6: _t->on_Connect_B_clicked(); break;
        case 7: _t->on_Select_Cust_clicked(); break;
        case 8: _t->on_refresh_B_clicked(); break;
        case 9: _t->on_ID_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject total_main::staticMetaObject = { {
    QMetaObject::SuperData::link<QDialog::staticMetaObject>(),
    qt_meta_stringdata_total_main.data,
    qt_meta_data_total_main,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *total_main::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *total_main::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_total_main.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int total_main::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
