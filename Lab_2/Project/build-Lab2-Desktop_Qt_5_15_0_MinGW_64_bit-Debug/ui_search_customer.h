/********************************************************************************
** Form generated from reading UI file 'search_customer.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEARCH_CUSTOMER_H
#define UI_SEARCH_CUSTOMER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>

QT_BEGIN_NAMESPACE

class Ui_search_customer
{
public:
    QGroupBox *INFO_OUT;
    QSplitter *Second_name;
    QLabel *second_name_L;
    QLabel *second_name_out;
    QSplitter *Date_of_birth;
    QLabel *data_L;
    QLabel *data_out;
    QSplitter *NIk;
    QLabel *nik_L;
    QLabel *nik_out;
    QSplitter *EMAIL;
    QLabel *email_L;
    QLabel *email_out;
    QSplitter *First_Name;
    QLabel *name_L;
    QLabel *name_out;
    QSplitter *ID;
    QLabel *ID_L;
    QLabel *ID_out;
    QLabel *logo;
    QPushButton *Exit_B;
    QGroupBox *INFO_IN;
    QSplitter *splitter;
    QLabel *name_in_L;
    QLineEdit *name_in;
    QSplitter *splitter_2;
    QLabel *second_name_in_L;
    QLineEdit *second_name_in;
    QSplitter *splitter_3;
    QLabel *ID_IN_L;
    QLineEdit *ID_IN;
    QPushButton *search_B;
    QPushButton *Clear_B;
    QSplitter *splitter_4;
    QLabel *stat_L;
    QLabel *stat_info;

    void setupUi(QDialog *search_customer)
    {
        if (search_customer->objectName().isEmpty())
            search_customer->setObjectName(QString::fromUtf8("search_customer"));
        search_customer->resize(800, 480);
        search_customer->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 0, 48);\n"
""));
        INFO_OUT = new QGroupBox(search_customer);
        INFO_OUT->setObjectName(QString::fromUtf8("INFO_OUT"));
        INFO_OUT->setGeometry(QRect(20, 180, 511, 281));
        QFont font;
        font.setPointSize(15);
        font.setBold(true);
        font.setWeight(75);
        INFO_OUT->setFont(font);
        INFO_OUT->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);"));
        Second_name = new QSplitter(INFO_OUT);
        Second_name->setObjectName(QString::fromUtf8("Second_name"));
        Second_name->setGeometry(QRect(10, 70, 491, 23));
        Second_name->setOrientation(Qt::Horizontal);
        second_name_L = new QLabel(Second_name);
        second_name_L->setObjectName(QString::fromUtf8("second_name_L"));
        second_name_L->setEnabled(true);
        second_name_L->setMinimumSize(QSize(187, 23));
        second_name_L->setMaximumSize(QSize(313, 23));
        QFont font1;
        font1.setPointSize(14);
        second_name_L->setFont(font1);
        second_name_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        Second_name->addWidget(second_name_L);
        second_name_out = new QLabel(Second_name);
        second_name_out->setObjectName(QString::fromUtf8("second_name_out"));
        second_name_out->setMinimumSize(QSize(300, 0));
        second_name_out->setFont(font1);
        second_name_out->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        Second_name->addWidget(second_name_out);
        Date_of_birth = new QSplitter(INFO_OUT);
        Date_of_birth->setObjectName(QString::fromUtf8("Date_of_birth"));
        Date_of_birth->setGeometry(QRect(10, 100, 491, 21));
        Date_of_birth->setOrientation(Qt::Horizontal);
        data_L = new QLabel(Date_of_birth);
        data_L->setObjectName(QString::fromUtf8("data_L"));
        data_L->setEnabled(true);
        data_L->setMinimumSize(QSize(187, 21));
        data_L->setMaximumSize(QSize(313, 16777215));
        data_L->setFont(font1);
        data_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        Date_of_birth->addWidget(data_L);
        data_out = new QLabel(Date_of_birth);
        data_out->setObjectName(QString::fromUtf8("data_out"));
        data_out->setMinimumSize(QSize(300, 0));
        data_out->setFont(font1);
        data_out->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        Date_of_birth->addWidget(data_out);
        NIk = new QSplitter(INFO_OUT);
        NIk->setObjectName(QString::fromUtf8("NIk"));
        NIk->setGeometry(QRect(10, 130, 491, 23));
        NIk->setOrientation(Qt::Horizontal);
        nik_L = new QLabel(NIk);
        nik_L->setObjectName(QString::fromUtf8("nik_L"));
        nik_L->setEnabled(true);
        nik_L->setMinimumSize(QSize(187, 23));
        nik_L->setMaximumSize(QSize(313, 16777215));
        nik_L->setFont(font1);
        nik_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        NIk->addWidget(nik_L);
        nik_out = new QLabel(NIk);
        nik_out->setObjectName(QString::fromUtf8("nik_out"));
        nik_out->setMinimumSize(QSize(300, 0));
        nik_out->setFont(font1);
        nik_out->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        NIk->addWidget(nik_out);
        EMAIL = new QSplitter(INFO_OUT);
        EMAIL->setObjectName(QString::fromUtf8("EMAIL"));
        EMAIL->setGeometry(QRect(10, 160, 491, 23));
        EMAIL->setOrientation(Qt::Horizontal);
        email_L = new QLabel(EMAIL);
        email_L->setObjectName(QString::fromUtf8("email_L"));
        email_L->setEnabled(true);
        email_L->setMinimumSize(QSize(187, 23));
        email_L->setMaximumSize(QSize(313, 16777215));
        email_L->setFont(font1);
        email_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        EMAIL->addWidget(email_L);
        email_out = new QLabel(EMAIL);
        email_out->setObjectName(QString::fromUtf8("email_out"));
        email_out->setMinimumSize(QSize(300, 0));
        email_out->setFont(font1);
        email_out->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        EMAIL->addWidget(email_out);
        First_Name = new QSplitter(INFO_OUT);
        First_Name->setObjectName(QString::fromUtf8("First_Name"));
        First_Name->setGeometry(QRect(10, 40, 491, 23));
        First_Name->setOrientation(Qt::Horizontal);
        name_L = new QLabel(First_Name);
        name_L->setObjectName(QString::fromUtf8("name_L"));
        name_L->setEnabled(true);
        name_L->setMinimumSize(QSize(187, 23));
        name_L->setMaximumSize(QSize(313, 16777215));
        name_L->setFont(font1);
        name_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        First_Name->addWidget(name_L);
        name_out = new QLabel(First_Name);
        name_out->setObjectName(QString::fromUtf8("name_out"));
        name_out->setMinimumSize(QSize(300, 23));
        name_out->setFont(font1);
        name_out->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        First_Name->addWidget(name_out);
        ID = new QSplitter(INFO_OUT);
        ID->setObjectName(QString::fromUtf8("ID"));
        ID->setGeometry(QRect(10, 190, 491, 23));
        ID->setOrientation(Qt::Horizontal);
        ID_L = new QLabel(ID);
        ID_L->setObjectName(QString::fromUtf8("ID_L"));
        ID_L->setEnabled(true);
        ID_L->setMinimumSize(QSize(187, 23));
        ID_L->setMaximumSize(QSize(313, 16777215));
        ID_L->setFont(font1);
        ID_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        ID->addWidget(ID_L);
        ID_out = new QLabel(ID);
        ID_out->setObjectName(QString::fromUtf8("ID_out"));
        ID_out->setMinimumSize(QSize(300, 0));
        ID_out->setFont(font1);
        ID_out->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        ID->addWidget(ID_out);
        logo = new QLabel(search_customer);
        logo->setObjectName(QString::fromUtf8("logo"));
        logo->setGeometry(QRect(50, 20, 698, 81));
        QFont font2;
        font2.setPointSize(50);
        font2.setBold(true);
        font2.setWeight(75);
        logo->setFont(font2);
        logo->setStyleSheet(QString::fromUtf8("color:white;"));
        Exit_B = new QPushButton(search_customer);
        Exit_B->setObjectName(QString::fromUtf8("Exit_B"));
        Exit_B->setGeometry(QRect(20, 110, 81, 21));
        QFont font3;
        font3.setPointSize(14);
        font3.setBold(true);
        font3.setWeight(75);
        Exit_B->setFont(font3);
        Exit_B->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        INFO_IN = new QGroupBox(search_customer);
        INFO_IN->setObjectName(QString::fromUtf8("INFO_IN"));
        INFO_IN->setGeometry(QRect(560, 180, 221, 161));
        QFont font4;
        font4.setPointSize(15);
        INFO_IN->setFont(font4);
        INFO_IN->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);"));
        splitter = new QSplitter(INFO_IN);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setGeometry(QRect(10, 40, 201, 26));
        splitter->setOrientation(Qt::Horizontal);
        name_in_L = new QLabel(splitter);
        name_in_L->setObjectName(QString::fromUtf8("name_in_L"));
        name_in_L->setMinimumSize(QSize(80, 0));
        QFont font5;
        font5.setPointSize(11);
        font5.setBold(true);
        font5.setWeight(75);
        name_in_L->setFont(font5);
        name_in_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter->addWidget(name_in_L);
        name_in = new QLineEdit(splitter);
        name_in->setObjectName(QString::fromUtf8("name_in"));
        name_in->setMaximumSize(QSize(117, 26));
        QFont font6;
        font6.setPointSize(11);
        name_in->setFont(font6);
        name_in->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"\n"
"\n"
""));
        splitter->addWidget(name_in);
        splitter_2 = new QSplitter(INFO_IN);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setGeometry(QRect(10, 80, 201, 26));
        splitter_2->setOrientation(Qt::Horizontal);
        second_name_in_L = new QLabel(splitter_2);
        second_name_in_L->setObjectName(QString::fromUtf8("second_name_in_L"));
        second_name_in_L->setMinimumSize(QSize(80, 0));
        second_name_in_L->setFont(font5);
        second_name_in_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_2->addWidget(second_name_in_L);
        second_name_in = new QLineEdit(splitter_2);
        second_name_in->setObjectName(QString::fromUtf8("second_name_in"));
        second_name_in->setMaximumSize(QSize(117, 26));
        second_name_in->setFont(font6);
        second_name_in->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"\n"
"\n"
""));
        splitter_2->addWidget(second_name_in);
        splitter_3 = new QSplitter(INFO_IN);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setGeometry(QRect(10, 120, 201, 26));
        splitter_3->setOrientation(Qt::Horizontal);
        ID_IN_L = new QLabel(splitter_3);
        ID_IN_L->setObjectName(QString::fromUtf8("ID_IN_L"));
        ID_IN_L->setMinimumSize(QSize(80, 0));
        ID_IN_L->setFont(font5);
        ID_IN_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_3->addWidget(ID_IN_L);
        ID_IN = new QLineEdit(splitter_3);
        ID_IN->setObjectName(QString::fromUtf8("ID_IN"));
        ID_IN->setMaximumSize(QSize(117, 26));
        ID_IN->setFont(font6);
        ID_IN->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"\n"
"\n"
""));
        splitter_3->addWidget(ID_IN);
        search_B = new QPushButton(search_customer);
        search_B->setObjectName(QString::fromUtf8("search_B"));
        search_B->setGeometry(QRect(700, 350, 81, 31));
        search_B->setFont(font3);
        search_B->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"\n"
"\n"
""));
        Clear_B = new QPushButton(search_customer);
        Clear_B->setObjectName(QString::fromUtf8("Clear_B"));
        Clear_B->setGeometry(QRect(560, 350, 111, 31));
        Clear_B->setFont(font3);
        Clear_B->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"\n"
"\n"
""));
        splitter_4 = new QSplitter(search_customer);
        splitter_4->setObjectName(QString::fromUtf8("splitter_4"));
        splitter_4->setGeometry(QRect(20, 140, 326, 23));
        splitter_4->setOrientation(Qt::Horizontal);
        stat_L = new QLabel(splitter_4);
        stat_L->setObjectName(QString::fromUtf8("stat_L"));
        stat_L->setMaximumSize(QSize(84, 16777215));
        stat_L->setFont(font3);
        stat_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_4->addWidget(stat_L);
        stat_info = new QLabel(splitter_4);
        stat_info->setObjectName(QString::fromUtf8("stat_info"));
        stat_info->setMinimumSize(QSize(0, 21));
        stat_info->setMaximumSize(QSize(238, 21));
        QFont font7;
        font7.setPointSize(10);
        font7.setBold(true);
        font7.setWeight(75);
        stat_info->setFont(font7);
        stat_info->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_4->addWidget(stat_info);

        retranslateUi(search_customer);

        QMetaObject::connectSlotsByName(search_customer);
    } // setupUi

    void retranslateUi(QDialog *search_customer)
    {
        search_customer->setWindowTitle(QCoreApplication::translate("search_customer", "Dialog", nullptr));
        INFO_OUT->setTitle(QCoreApplication::translate("search_customer", "\320\206\320\275\321\204\320\276\321\200\320\274\320\260\321\206\321\226\321\217", nullptr));
        second_name_L->setText(QCoreApplication::translate("search_customer", "\320\237\321\200\321\226\320\267\320\262\320\270\321\211\320\265:", nullptr));
        second_name_out->setText(QString());
        data_L->setText(QCoreApplication::translate("search_customer", "\320\224\320\260\321\202\320\260 \320\275\320\260\321\200\320\276\320\264\320\266\320\265\320\275\320\275\321\217:", nullptr));
        data_out->setText(QString());
        nik_L->setText(QCoreApplication::translate("search_customer", "\320\235\321\226\320\272\320\275\320\265\320\271\320\274:", nullptr));
        nik_out->setText(QString());
        email_L->setText(QCoreApplication::translate("search_customer", "\320\225\320\273\320\265\320\272\321\202\321\200\320\276\320\275\320\275\320\260 \320\277\320\276\321\210\321\202\320\260:", nullptr));
        email_out->setText(QString());
        name_L->setText(QCoreApplication::translate("search_customer", "\320\206\320\274'\321\217:", nullptr));
        name_out->setText(QString());
        ID_L->setText(QCoreApplication::translate("search_customer", "ID:", nullptr));
        ID_out->setText(QString());
        logo->setText(QCoreApplication::translate("search_customer", "\320\237\320\276\321\210\321\203\320\272 \320\232\320\276\321\200\320\270\321\201\321\202\321\203\320\262\320\260\321\207\320\260", nullptr));
        Exit_B->setText(QCoreApplication::translate("search_customer", "<--", nullptr));
        INFO_IN->setTitle(QCoreApplication::translate("search_customer", "\320\222\320\262\320\265\320\264\321\226\321\202\321\214 \320\264\320\260\320\275\321\226:", nullptr));
        name_in_L->setText(QCoreApplication::translate("search_customer", "\320\206\320\274'\321\217:", nullptr));
        second_name_in_L->setText(QCoreApplication::translate("search_customer", "\320\237\321\200\321\226\320\267\320\262\320\270\321\211\320\265:", nullptr));
        ID_IN_L->setText(QCoreApplication::translate("search_customer", "ID:", nullptr));
        search_B->setText(QCoreApplication::translate("search_customer", "\320\237\320\276\321\210\321\203\320\272", nullptr));
        Clear_B->setText(QCoreApplication::translate("search_customer", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\320\270", nullptr));
        stat_L->setText(QCoreApplication::translate("search_customer", " \320\241\321\202\320\260\321\202\321\203\321\201: ", nullptr));
        stat_info->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class search_customer: public Ui_search_customer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEARCH_CUSTOMER_H
