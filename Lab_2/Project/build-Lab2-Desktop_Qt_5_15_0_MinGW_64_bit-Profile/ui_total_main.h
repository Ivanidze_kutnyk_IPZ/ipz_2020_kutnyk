/********************************************************************************
** Form generated from reading UI file 'total_main.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TOTAL_MAIN_H
#define UI_TOTAL_MAIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_total_main
{
public:
    QLabel *logo;
    QGroupBox *groupBox;
    QPushButton *Search_b;
    QPushButton *EXIT_B;
    QLabel *contacts_logo;
    QLabel *line;
    QTextEdit *sms_text;
    QPushButton *Clear_b;
    QPushButton *Send_b;
    QTextBrowser *ALL_LABEL;
    QLabel *contacts_logo_2;
    QPushButton *Connect_B;
    QPushButton *Select_Cust;
    QGroupBox *INFO_IN;
    QSplitter *splitter;
    QLabel *name_in_L;
    QLineEdit *name_in;
    QSplitter *splitter_2;
    QLabel *second_name_in_L;
    QLineEdit *second_name_in;
    QLabel *label;
    QPushButton *refresh_B;
    QSplitter *splitter_3;
    QLabel *SEL_name;
    QLabel *SEL_sec_name;
    QTextBrowser *ALL_CUST;
    QLabel *label_2;
    QLabel *KORUSTUBACHI;
    QPushButton *ID;

    void setupUi(QDialog *total_main)
    {
        if (total_main->objectName().isEmpty())
            total_main->setObjectName(QString::fromUtf8("total_main"));
        total_main->resize(800, 480);
        total_main->setFocusPolicy(Qt::TabFocus);
        total_main->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 0, 48);\n"
""));
        logo = new QLabel(total_main);
        logo->setObjectName(QString::fromUtf8("logo"));
        logo->setGeometry(QRect(0, 0, 41, 54));
        QFont font;
        font.setFamily(QString::fromUtf8("Vivaldi"));
        font.setPointSize(26);
        font.setBold(true);
        font.setWeight(75);
        logo->setFont(font);
        logo->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);\n"
"border::rgb(0, 0, 90);"));
        groupBox = new QGroupBox(total_main);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(0, 50, 41, 431));
        groupBox->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);\n"
"border:black;"));
        Search_b = new QPushButton(groupBox);
        Search_b->setObjectName(QString::fromUtf8("Search_b"));
        Search_b->setGeometry(QRect(0, 10, 41, 41));
        QFont font1;
        font1.setPointSize(20);
        font1.setBold(true);
        font1.setWeight(75);
        Search_b->setFont(font1);
        Search_b->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        EXIT_B = new QPushButton(groupBox);
        EXIT_B->setObjectName(QString::fromUtf8("EXIT_B"));
        EXIT_B->setGeometry(QRect(0, 55, 41, 41));
        EXIT_B->setFont(font1);
        EXIT_B->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        contacts_logo = new QLabel(total_main);
        contacts_logo->setObjectName(QString::fromUtf8("contacts_logo"));
        contacts_logo->setGeometry(QRect(40, 0, 281, 41));
        QFont font2;
        font2.setPointSize(15);
        font2.setBold(true);
        font2.setWeight(75);
        contacts_logo->setFont(font2);
        contacts_logo->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);\n"
"border::rgb(0, 0, 90);"));
        line = new QLabel(total_main);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(320, 0, 21, 481));
        line->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);\n"
"border::rgb(0, 0, 90);"));
        sms_text = new QTextEdit(total_main);
        sms_text->setObjectName(QString::fromUtf8("sms_text"));
        sms_text->setGeometry(QRect(350, 400, 361, 70));
        sms_text->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        Clear_b = new QPushButton(total_main);
        Clear_b->setObjectName(QString::fromUtf8("Clear_b"));
        Clear_b->setGeometry(QRect(719, 420, 71, 21));
        Clear_b->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        Send_b = new QPushButton(total_main);
        Send_b->setObjectName(QString::fromUtf8("Send_b"));
        Send_b->setGeometry(QRect(720, 450, 71, 21));
        Send_b->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        ALL_LABEL = new QTextBrowser(total_main);
        ALL_LABEL->setObjectName(QString::fromUtf8("ALL_LABEL"));
        ALL_LABEL->setGeometry(QRect(350, 50, 441, 331));
        ALL_LABEL->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        contacts_logo_2 = new QLabel(total_main);
        contacts_logo_2->setObjectName(QString::fromUtf8("contacts_logo_2"));
        contacts_logo_2->setGeometry(QRect(340, 0, 461, 41));
        contacts_logo_2->setFont(font2);
        contacts_logo_2->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);\n"
"border::rgb(0, 0, 90);"));
        Connect_B = new QPushButton(total_main);
        Connect_B->setObjectName(QString::fromUtf8("Connect_B"));
        Connect_B->setGeometry(QRect(710, 10, 80, 21));
        Connect_B->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        Select_Cust = new QPushButton(total_main);
        Select_Cust->setObjectName(QString::fromUtf8("Select_Cust"));
        Select_Cust->setGeometry(QRect(150, 180, 161, 21));
        Select_Cust->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        INFO_IN = new QGroupBox(total_main);
        INFO_IN->setObjectName(QString::fromUtf8("INFO_IN"));
        INFO_IN->setGeometry(QRect(50, 50, 261, 121));
        QFont font3;
        font3.setPointSize(15);
        INFO_IN->setFont(font3);
        INFO_IN->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);"));
        splitter = new QSplitter(INFO_IN);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setGeometry(QRect(30, 40, 201, 26));
        splitter->setOrientation(Qt::Horizontal);
        name_in_L = new QLabel(splitter);
        name_in_L->setObjectName(QString::fromUtf8("name_in_L"));
        name_in_L->setMinimumSize(QSize(80, 0));
        QFont font4;
        font4.setPointSize(11);
        font4.setBold(true);
        font4.setWeight(75);
        name_in_L->setFont(font4);
        name_in_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter->addWidget(name_in_L);
        name_in = new QLineEdit(splitter);
        name_in->setObjectName(QString::fromUtf8("name_in"));
        name_in->setMaximumSize(QSize(117, 26));
        QFont font5;
        font5.setPointSize(11);
        name_in->setFont(font5);
        name_in->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"\n"
"\n"
""));
        splitter->addWidget(name_in);
        splitter_2 = new QSplitter(INFO_IN);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setGeometry(QRect(30, 80, 201, 26));
        splitter_2->setOrientation(Qt::Horizontal);
        second_name_in_L = new QLabel(splitter_2);
        second_name_in_L->setObjectName(QString::fromUtf8("second_name_in_L"));
        second_name_in_L->setMinimumSize(QSize(80, 0));
        second_name_in_L->setFont(font4);
        second_name_in_L->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);"));
        splitter_2->addWidget(second_name_in_L);
        second_name_in = new QLineEdit(splitter_2);
        second_name_in->setObjectName(QString::fromUtf8("second_name_in"));
        second_name_in->setMaximumSize(QSize(117, 26));
        second_name_in->setFont(font5);
        second_name_in->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"\n"
"\n"
""));
        splitter_2->addWidget(second_name_in);
        label = new QLabel(total_main);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(40, 210, 281, 16));
        label->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);\n"
"border::rgb(0, 0, 90);"));
        refresh_B = new QPushButton(total_main);
        refresh_B->setObjectName(QString::fromUtf8("refresh_B"));
        refresh_B->setGeometry(QRect(600, 10, 51, 21));
        refresh_B->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        splitter_3 = new QSplitter(total_main);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setGeometry(QRect(410, 10, 182, 21));
        splitter_3->setOrientation(Qt::Horizontal);
        SEL_name = new QLabel(splitter_3);
        SEL_name->setObjectName(QString::fromUtf8("SEL_name"));
        SEL_name->setMinimumSize(QSize(91, 21));
        SEL_name->setMaximumSize(QSize(91, 21));
        SEL_name->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        splitter_3->addWidget(SEL_name);
        SEL_sec_name = new QLabel(splitter_3);
        SEL_sec_name->setObjectName(QString::fromUtf8("SEL_sec_name"));
        SEL_sec_name->setMinimumSize(QSize(91, 21));
        SEL_sec_name->setMaximumSize(QSize(91, 21));
        SEL_sec_name->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        splitter_3->addWidget(SEL_sec_name);
        ALL_CUST = new QTextBrowser(total_main);
        ALL_CUST->setObjectName(QString::fromUtf8("ALL_CUST"));
        ALL_CUST->setGeometry(QRect(50, 271, 261, 181));
        ALL_CUST->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        label_2 = new QLabel(total_main);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(40, 460, 281, 21));
        label_2->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 90);\n"
"border::rgb(0, 0, 90);"));
        KORUSTUBACHI = new QLabel(total_main);
        KORUSTUBACHI->setObjectName(QString::fromUtf8("KORUSTUBACHI"));
        KORUSTUBACHI->setGeometry(QRect(50, 229, 261, 31));
        QFont font6;
        font6.setPointSize(12);
        font6.setBold(true);
        font6.setWeight(75);
        KORUSTUBACHI->setFont(font6);
        KORUSTUBACHI->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);"));
        ID = new QPushButton(total_main);
        ID->setObjectName(QString::fromUtf8("ID"));
        ID->setGeometry(QRect(655, 10, 51, 21));
        ID->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(0, 0, 140);\n"
"border::rgb(0, 0, 140);\n"
""));

        retranslateUi(total_main);

        QMetaObject::connectSlotsByName(total_main);
    } // setupUi

    void retranslateUi(QDialog *total_main)
    {
        total_main->setWindowTitle(QCoreApplication::translate("total_main", "Dialog", nullptr));
        logo->setText(QCoreApplication::translate("total_main", " fg", nullptr));
        groupBox->setTitle(QString());
        Search_b->setText(QCoreApplication::translate("total_main", "\320\237", nullptr));
        EXIT_B->setText(QCoreApplication::translate("total_main", "\320\222", nullptr));
        contacts_logo->setText(QCoreApplication::translate("total_main", "              \320\232\320\276\320\275\321\202\320\260\320\272\321\202\320\270", nullptr));
        line->setText(QString());
        Clear_b->setText(QCoreApplication::translate("total_main", "--> X", nullptr));
        Send_b->setText(QCoreApplication::translate("total_main", "-->", nullptr));
        contacts_logo_2->setText(QString());
        Connect_B->setText(QCoreApplication::translate("total_main", "Connect", nullptr));
        Select_Cust->setText(QCoreApplication::translate("total_main", "\320\236\320\261\321\200\320\260\321\202\320\270 \320\232\320\276\321\200\320\270\321\201\321\202\321\203\320\262\320\260\321\207\320\260", nullptr));
        INFO_IN->setTitle(QCoreApplication::translate("total_main", "\320\222\320\262\320\265\320\264\321\226\321\202\321\214 \320\264\320\260\320\275\321\226:", nullptr));
        name_in_L->setText(QCoreApplication::translate("total_main", "\320\206\320\274'\321\217:", nullptr));
        second_name_in_L->setText(QCoreApplication::translate("total_main", "\320\237\321\200\321\226\320\267\320\262\320\270\321\211\320\265:", nullptr));
        label->setText(QString());
        refresh_B->setText(QCoreApplication::translate("total_main", "Refresh", nullptr));
        SEL_name->setText(QString());
        SEL_sec_name->setText(QString());
        label_2->setText(QString());
        KORUSTUBACHI->setText(QCoreApplication::translate("total_main", "              \320\232\320\236\320\240\320\230\320\241\320\242\320\243\320\222\320\220\320\247\320\206", nullptr));
        ID->setText(QCoreApplication::translate("total_main", "Identyfy", nullptr));
    } // retranslateUi

};

namespace Ui {
    class total_main: public Ui_total_main {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TOTAL_MAIN_H
