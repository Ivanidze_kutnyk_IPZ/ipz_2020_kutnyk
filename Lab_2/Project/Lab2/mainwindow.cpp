#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "registration.h"
#include "total_main.h"
#include "QMessageBox"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// Кнопка ВІДОБРАЖЕННЯ інформації про ПО
void MainWindow::on_About_Button_clicked()
{
    QMessageBox *msg_ab = new QMessageBox();
    msg_ab->setStyleSheet("background-color:rgb(0, 0, 120); color:white;");
    msg_ab->setWindowTitle("Інформація");
    msg_ab->setText("Вас вітає FreeGram, v1.0  зборка 0.000030");
    msg_ab->setInformativeText("Робота студента гр. КІ-33, Кутник І.П.");
    msg_ab->open();

}
// Кнопка ВИХІД з початокового меню
void MainWindow::on_EXIT_Button_clicked()
{
    this->close();
}
// Кнопка Авторизація + перевірка авторизації
void MainWindow::on_IN_b_clicked()
{
    total_main win_3;
    win_3.setModal(true);
    win_3.setMinimumSize(800,480);
    win_3.setMaximumSize(800,480);
    win_3.setWindowTitle("FreeGram");

    //Операнди ВВОДУ даних
    QString login_l = ui->login_l->text();
    QString password_l = ui->pass_l->text();
    //Налаштування Меседж Боксу
    QMessageBox *log_info = new QMessageBox();
     log_info->setStyleSheet("background-color:rgb(0, 0, 120); color:white;");
    //Перевірка на правильність результату
    if(login_l == "TEST" && password_l == "1234")
    {
       hide();
       win_3.exec();
       }else {

        log_info->setWindowTitle("Помилка");
        log_info->setText("Помилка авторизації");
        log_info->open();
         }
}
// Кнопка ВІДКРИТИ вікно рєстрації
void MainWindow::on_Reg_b_clicked()
{
    //hide();
    registration win_2;
    win_2.setModal(true);
    win_2.setMinimumSize(800,480);
    win_2.setMaximumSize(800,480);
    win_2.setWindowTitle("Registration");
    win_2.exec();
}
