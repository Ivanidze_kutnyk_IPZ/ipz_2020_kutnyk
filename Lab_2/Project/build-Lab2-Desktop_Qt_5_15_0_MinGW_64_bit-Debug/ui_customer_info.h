/********************************************************************************
** Form generated from reading UI file 'customer_info.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CUSTOMER_INFO_H
#define UI_CUSTOMER_INFO_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_customer_info
{
public:
    QWidget *centralwidget;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *customer_info)
    {
        if (customer_info->objectName().isEmpty())
            customer_info->setObjectName(QString::fromUtf8("customer_info"));
        customer_info->resize(800, 600);
        centralwidget = new QWidget(customer_info);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        customer_info->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(customer_info);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        customer_info->setStatusBar(statusbar);

        retranslateUi(customer_info);

        QMetaObject::connectSlotsByName(customer_info);
    } // setupUi

    void retranslateUi(QMainWindow *customer_info)
    {
        customer_info->setWindowTitle(QCoreApplication::translate("customer_info", "MainWindow", nullptr));
    } // retranslateUi

};

namespace Ui {
    class customer_info: public Ui_customer_info {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CUSTOMER_INFO_H
